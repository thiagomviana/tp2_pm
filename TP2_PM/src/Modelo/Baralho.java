package Modelo;

import java.util.ArrayList;
import java.util.Collections;

public class Baralho {

    private ArrayList<Carta> cartas;
    private ArrayList<Carta> mesa;
    private ArrayList<Carta> jogadas;
    private final ArrayList<String> cores;
    private static Baralho instancia;

    @SuppressWarnings("empty-statement")
    public Baralho() {
        cartas = new ArrayList<>();
        cores = new ArrayList<>();
        mesa = new ArrayList<>();
        jogadas = new ArrayList<>();
        cores.add("Amarelo");
        cores.add("Azul");
        cores.add("Verde");
        cores.add("Vermelho");

        // so existe uma carta zero de cada cor no baralho do UNO
        for (String core : cores) {
            cartas.add(new Carta(core, String.valueOf(0), false));
        }

        //existem no baralho duas de cada das outras cartas
        for (int i = 0; i < 8; i++) {

            for (int j = 1; j <= 9; j++) {
                cartas.add(new Carta(cores.get(i % cores.size()), String.valueOf(j), false));
            }

            cartas.add(new Carta(cores.get(i % cores.size()), "Pular", true));
            cartas.add(new Carta(cores.get(i % cores.size()), "Reverter", true));
            cartas.add(new Carta(cores.get(i % cores.size()), "+2", true));

            cartas.add(new Carta("", "Coringa", true));
            cartas.add(new Carta("", "+4", true));
        }
    }

    public static Baralho getInstance() { //obtem a instancia do Singleton
        if (instancia == null) {
            instancia = new Baralho();
        }

        return instancia;
    }

    public ArrayList<String> getCores() {
        return cores;
    }

    public ArrayList<Carta> getJogadas() {
        return jogadas;
    }

    public Carta getUltimaCartaJogada() {
        return jogadas.get(jogadas.size() - 1);
    }

    public void embaralhar() {
        cartas.addAll(mesa);
        cartas.addAll(jogadas);

        Collections.shuffle(cartas);
    }

    public ArrayList darCartas(int numCartas) { //da uma mao de numCartas cartas
        ArrayList<Carta> mao = new ArrayList<>();
        Carta carta;

        for (int i = 0; i < numCartas; i++) {
            carta = cartas.get(0);

            mao.add(carta);
            cartas.remove(carta);
        }

        return mao;
    }

    public void addJogada(Carta carta) { //adiciona uma carta ao monte de cartas que já foram jogadas
        jogadas.add(carta);
    }

    public Carta colocarPrimeiraCarta() {
        Carta carta;
        mesa = cartas;

        do {
            carta = mesa.get(0);
            mesa.remove(carta);

            addJogada(carta);
        } while (carta.isCartaDeAcao());

        return carta;
    }

    public Carta comprar() { //retorna uma carta que será comprada do monte virado para baixo da mesa
        Carta carta;

        if (mesa.size() <= 0) {
            mesa.addAll(jogadas);
        }
        carta = mesa.get(0);
        mesa.remove(carta);

        return carta;
    }

}
