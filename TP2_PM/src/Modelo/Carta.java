package Modelo;

public class Carta {

    private String cor;
    private String nome;
    private boolean cartaDeAcao;

    public Carta(String cor, String nome, boolean cartaDeAcao) {
        this.cor = cor;
        this.nome = nome;
        this.cartaDeAcao = cartaDeAcao;
    }

    public String getCor() {
        return cor;
    }

    public String getNome() {
        return nome;
    }

    public boolean isCartaDeAcao() {
        return cartaDeAcao;
    }

    @Override
    public String toString() {
        return nome + " " + cor;
    }

}
