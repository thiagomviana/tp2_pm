package Modelo;

import java.util.ArrayList;

public class Jogador {

    private String nome;
    private ArrayList<Carta> mao;
    private int pontos;

    public Jogador(String nome) {
        this.nome = nome;
        pontos = 0;
        mao = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public ArrayList<Carta> getMao() {
        return mao;
    }

    public void setMao(ArrayList<Carta> mao) {
        this.mao = mao;
    }

    public int getPontos() {
        return pontos;
    }

    public void acrescentarPontos(int pontos) {
        this.pontos += pontos;
    }

    public int getNumCartasNaMao() {
        return mao.size();
    }

    public int getCartaDaMao(String nome) { //procura pelo indice de uma carta de determinado nome. Caso o jogador não tenha essa carta, retorna -1
        for (Carta c : mao) {
            if (c.getNome().equals(nome)) {
                return mao.indexOf(c);
            }
        }

        return -1;
    }

    public boolean temCor(String cor) { //caso o jogador tenha cartas de determinada cor, retorna verdadeiro, caso contrário, falso
        for (Carta c : mao) {
            if (c.getCor().equals(cor)) {
                return true;
            }
        }

        return false;
    }

    public boolean temCartaCompativel(Carta carta) { //se o jogador tem uma carta que pode ser jogada se uma determinada carta estiver sobre a mesa
        for (Carta c : mao) {
            if (c.getCor().equals("")) {
                return true;
            }
            if (c.getCor().equals(carta.getCor())) {
                return true;
            }
            if (c.getNome().equals(carta.getNome())) {
                return true;
            }
        }

        return false;
    }
    
    //retorna o índice da primeira carta compatível com a carta de compra lançada, caso o jogador não tenha nenhuma, retorna -1
    public int temCartaCompativelStack(Carta carta) { 
        for (Carta c : mao) {            
            if (c.getNome().equals(carta.getNome())) {
                return mao.lastIndexOf(c);
            };
        }

        return -1;
    }

    public void comprar(Carta carta) {
        mao.add(carta);
    }

    public Carta jogar(int indice) {
        Carta carta = mao.get(indice);
        mao.remove(carta);

        return carta;
    }
}
