/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visao;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author tviana
 */
public interface Visao {
    //mostra os dialogos quando um jogador recebe uma carta de ação para comprar cartas
    public void mostrarDialogoJogadorStackCompra(String nomeJogadorDaVez, String nomeUltimaCartaJogada, String corUltimaCartaJogada, boolean temCartaStack, int numCartasStack); 

    public void gritarUNO(String jogador); //jogador com uma carta grita UNO

    public void mostrarMsgNovaRodada(HashMap<String, Integer> placar); //mostra o placar atual, ao final de cada rodada do jogo normal

    public void mostrarGanhadorDoJogo(String ganhador); //mostra o ganhador do jogo

    public void mostrarGanhadorDaRodada(String ganhador); //mostra o ganhador da rodada

    public void mostrarDialogoCompraCarta(String carta); //avisa ao jogador que a compra é válida para jogar
    
    //mostra a mão do jogador e as opções de jogada que ele possui
    public String mostrarDialogoJogador(String nomeUltimaCartaJogada, String corVigente, String nomeJogador, ArrayList<String> maoJogador, boolean temCartaValida, boolean ultimaOpcaoValida, boolean ultimaJogadaValida);

    public String mostrarDialogoEscolhaCor(ArrayList<String> cores); //ao jogar uma carta curinga, mostra as opções de cores, e permite que o usuário escolha uma cor

    public ArrayList<String> mostrarDialogoCadastroJogadores(); //faz o dialogo de cadastro dos jogadores, ao início do jogo

    public void mostrarMsgBoasVindas(); //mostra a mensagem de boas vindas no início do jogo

    public boolean selecionarTipoDeJogo(); //mostra o diálogo de seleção, para que seja escolhido o tipo de jogo a ser jogado
}
