/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Thiago
 */
public class VisaoTerminal implements Visao {

    private Scanner sc;

    public VisaoTerminal() {
        sc = new Scanner(System.in);
    }

    @Override
    public void mostrarDialogoJogadorStackCompra(String nomeJogadorDaVez, String nomeUltimaCartaJogada, String corUltimaCartaJogada, boolean temCartaStack, int numCartasStack) {
        clrscr();

        System.out.println("É a vez de " + nomeJogadorDaVez);

        System.out.println("O último jogador lançou um " + nomeUltimaCartaJogada + " " + corUltimaCartaJogada);

        if (temCartaStack) {
            System.out.println("Você pode lançar seu " + nomeUltimaCartaJogada + ". Digite enter para jogá-la.");
        } else {
            System.out.println("Você não tem nenhum " + nomeUltimaCartaJogada + ". Digite enter para comprar as " + numCartasStack + " cartas acumuladas.");
        }

        getch();
    }

    @Override
    public void gritarUNO(String jogador) {
        System.out.println("Jogador " + jogador + " disse UNO!");
        getch();
    }

    @Override
    public void mostrarMsgNovaRodada(HashMap<String, Integer> placar) {
        System.out.println("Iniciando nova rodada.");
        System.out.println("Placar atual: ");

        for (String jogador : placar.keySet()) {
            System.out.println(jogador + ": " + placar.get(jogador));
        }

        System.out.println("------------- Nova Rodada -------------");
        getch();
        clrscr();
    }

    @Override
    public void mostrarGanhadorDoJogo(String ganhador) {
        clrscr();
        System.out.println("O jogador " + ganhador + " ganhou o jogo!");
    }

    @Override
    public void mostrarGanhadorDaRodada(String ganhador) {
        clrscr();
        System.out.println("O jogador " + ganhador + " ganhou a rodada!");
    }

    @Override
    public void mostrarDialogoCompraCarta(String carta) {
        System.out.println("Que sorte! Você comprou um " + carta + "! Digite enter para jogá-la.");
        getch();
    }

    @Override
    public String mostrarDialogoJogador(String nomeUltimaCartaJogada, String corVigente, String nomeJogador, ArrayList<String> maoJogador, boolean temCartaValida, boolean ultimaOpcaoValida, boolean ultimaJogadaValida) {
        int i = 0;
        String opcao;

        clrscr();

        if (!ultimaOpcaoValida) { //se a ultima opcao foi inválida, avisa ao jogador
            System.out.println("Opção inválida, digite novamente.");
        }
        if (!ultimaJogadaValida) { //se a ultima jogada foi inválida, avisa ao jogador
            System.out.println("Jogada inválida. Jogue um " + nomeUltimaCartaJogada + " " + corVigente + ", um curinga, um +4 ou uma carta da cor " + corVigente);
        }

        System.out.println("É a vez de " + nomeJogador);

        if (!nomeUltimaCartaJogada.equals("Coringa") && !nomeUltimaCartaJogada.equals("+4"))             
        {
            System.out.println("Última carta na mesa: " + nomeUltimaCartaJogada + " " + corVigente);
        } else { //se a ultima carta jogada foi uma carta Coringa, alerta a mudança da cor vigente
            System.out.println("Última carta na mesa: " + nomeUltimaCartaJogada);
            System.out.println("A cor foi mudada para " + corVigente);

        }

        System.out.println("Escolha a carta que vai jogar:");

        for (String c : maoJogador) { //imprime a mao do jogador na tela
            System.out.println(++i + ". " + c);
        }

        if (!temCartaValida) {
            System.out.println("Você não tem nenhuma carta da mesma cor ou número da ultima carta na mesa, digite 'c' para comprar.");
        } else {
            System.out.println("Para comprar uma carta, digite 'c'");
        }

        System.out.print("Digite sua opção: ");
        opcao = sc.next();

        return opcao;
    }

    @Override
    public String mostrarDialogoEscolhaCor(ArrayList<String> cores) {
        int opcao;
        int i = 0;

        System.out.println("Você jogou uma carta coringa. Escolha uma cor:");
        for (String cor : cores) {
            System.out.println(++i + ". " + cor);
        }

        System.out.print("\nSua opção: ");
        opcao = sc.nextInt();
        opcao--;

        if (opcao >= 0 && opcao < cores.size()) {
            return cores.get(opcao);
        }

        return null;
    }

    @Override
    public ArrayList<String> mostrarDialogoCadastroJogadores() {
        ArrayList<String> nomesJogadores = new ArrayList<>();
        int numJogadores;
        String nome;

        System.out.print("Informe o número de jogadores (no máximo 10): ");
        numJogadores = sc.nextInt();

        for (int i = 1; i <= numJogadores; i++) {
            System.out.print("Jogador " + i + ", digite seu nome: ");
            nome = sc.next();

            nomesJogadores.add(nome);
        }
        clrscr();

        return nomesJogadores;
    }

    private void clrscr() { // "limpa" a tela
        for (int i = 0; i < 22; i++) {
            System.out.println("");
        }
    }

    private void getch() { //aguarda o usuário digitar um Enter
        try {
            System.in.read();
        } catch (IOException ex) {
        }
    }

    @Override
    public void mostrarMsgBoasVindas() {
        System.out.println("Seja bem-vindo ao UNO PM 2015/1");
        System.out.println("-- Embaralhando e separando cartas --");
    }

    @Override
    public boolean selecionarTipoDeJogo() {
        String opcao;
        boolean opcaoValida = false;

        do {
            System.out.print("Digite 'r' para jogar um jogo rápido ou 'n' para jogar um jogo normal: ");
            opcao = sc.next();

            if (opcao.equals("r")) {
                return true;
            }
            if (opcao.equals("n")) {
                return false;
            }
        } while (!opcaoValida);
        return false;
    }
}
