package Controle;

import Modelo.Baralho;
import Modelo.Carta;
import Modelo.Jogador;
import Visao.Visao;
import Visao.VisaoTerminal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class Jogo {

    private boolean sentidoHorario;
    private Carta ultimaJogada;
    private ArrayList<Jogador> jogadores;
    private Jogador jogadorDaVez;
    private int indiceJogadorDaVez;
    private String corAtual;
    private int numCartasAComprar;
    private final int pontosObjetivo = 500;
    private final int numCartasMao = 7;
    private boolean jogoRapido;
    private Visao visao;

    public Jogo(boolean jogoRapido, Visao visao) {
        sentidoHorario = true;
        this.visao = visao;
        this.jogoRapido = jogoRapido;
        jogadores = new ArrayList<>();
        numCartasAComprar = 0;
    }

    private void iniciarJogo() { //executa as operações iniciais para o início de uma rodada do jogo
        Random random = new Random();
        ArrayList<String> nomesJogadores = null;

        numCartasAComprar = 0;
        sentidoHorario = true;

        if (jogadores.isEmpty()) {
            nomesJogadores = visao.mostrarDialogoCadastroJogadores();
            for (String nome : nomesJogadores) {
                jogadores.add(new Jogador(nome));
            }
        }

        Baralho.getInstance().embaralhar();

        for (Jogador jogador : jogadores) {
            jogador.setMao(Baralho.getInstance().darCartas(numCartasMao)); //da as cartas aos jogadores
        }

        ultimaJogada = Baralho.getInstance().colocarPrimeiraCarta();
        corAtual = ultimaJogada.getCor();

        indiceJogadorDaVez = random.nextInt(jogadores.size());
        jogadorDaVez = jogadores.get(indiceJogadorDaVez);
    }

    public void partida() {
        boolean ganhouRodada = false;
        Jogador ganhadorDaRodada = new Jogador("");
        HashMap<String, Integer> placar = new HashMap<>();
        iniciarJogo();

        do {
            Carta cartaJogada = dialogoJogador(); //obtem a carta jogada pelo jogador da vez

            if (cartaJogada != null) { //se o jogador NÃO tiver comprado uma carta ao invés de jogar
                if (jogadorDaVez.getNumCartasNaMao() == 1) {
                    visao.gritarUNO(jogadorDaVez.getNome());
                }

                Baralho.getInstance().addJogada(cartaJogada);
                interpretarJogada(cartaJogada);

                if (jogadorDaVez.getNumCartasNaMao() == 0) { //se acabarem as cartas do jogador, ele é o ganhador da rodada
                    ganhadorDaRodada = jogadorDaVez;
                    ganhouRodada = true;
                }
            }

            proximoJogador();
        } while (!ganhouRodada);

        if (ganhadorDaRodada.getPontos() >= pontosObjetivo || jogoRapido) { //se o jogador atingiu os 500 pontos (jogo normal) ou ficou sem cartas (jogo rápido) ele é o ganhador
            visao.mostrarGanhadorDoJogo(ganhadorDaRodada.getNome());
        } else { //caso contrário, conta os pontos e inicia uma nova rodada
            visao.mostrarGanhadorDaRodada(ganhadorDaRodada.getNome());
            contagemDePontos(ganhadorDaRodada);

            Baralho.getInstance().embaralhar();

            for (Jogador j : jogadores) {
                placar.put(j.getNome(), j.getPontos()); //carrega o placar do jogo para ser exibido
            }
            visao.mostrarMsgNovaRodada(placar); //mostra o placar dos jogadores e indica o início de uma nova rodada
            this.partida();
        }
    }

    private void contagemDePontos(Jogador ganhadorDaRodada) {
        ArrayList<Carta> mao;

        do {
            mao = jogadorDaVez.getMao();

            for (Carta c : mao) {
                switch (c.getNome()) {
                    case "Reverter":
                    case "Pular":
                    case "+2":
                        ganhadorDaRodada.acrescentarPontos(20); //essas cartas valem 20 pontos cada
                        break;

                    case "+4":
                    case "Coringa":
                        ganhadorDaRodada.acrescentarPontos(50); //essas cartas valem 50 pontos cada
                        break;

                    default:
                        ganhadorDaRodada.acrescentarPontos(Integer.valueOf(c.getNome())); //as Cartas de valor númerico de 0 a 9 possuem o mesmo valor do número que está marcado nela
                        break;
                }
            }

            Baralho.getInstance().getJogadas().addAll(mao);
            proximoJogador();

        } while (!jogadorDaVez.equals(ganhadorDaRodada));
    }

    private void proximoJogador() {
        if (sentidoHorario) { //passa para o próximo de acordo com o sentido do jogo
            indiceJogadorDaVez++;
        } else {
            indiceJogadorDaVez--;
        }
        
    //evita ArrayOutOfBoundsException
        if (indiceJogadorDaVez == jogadores.size()) {
            indiceJogadorDaVez = 0;
        }
        if (indiceJogadorDaVez < 0) {
            indiceJogadorDaVez = jogadores.size() - 1;
        }

        jogadorDaVez = jogadores.get(indiceJogadorDaVez); //muda o jogador da vez
    }

    private Carta dialogoJogador() {
        Scanner sc = new Scanner(System.in);
        String opcao = "";
        ArrayList<String> maoJogadorDaVez = new ArrayList<>();
        int indiceCarta;
        boolean opcaoValida = true;
        boolean ultimaJogadaValida = true;
        boolean temCartaValida;

        do {
            if (ultimaJogada.getCor().equals("")) { //se a ultima carta jogada for uma carta coringa, o jogador precisa ter qualquer carta da cor escolhida
                temCartaValida = jogadorDaVez.temCor(corAtual); 
            } else {
                temCartaValida = jogadorDaVez.temCartaCompativel(ultimaJogada);
            }

            for (Carta c : jogadorDaVez.getMao()) { //prepara a mao para ser exibida na visao
                maoJogadorDaVez.add(c.toString());
            }

            if (numCartasAComprar > 0) { //se a ultima carta lançada é uma carta +2 ou +4
                indiceCarta = jogadorDaVez.temCartaCompativelStack(ultimaJogada);
                visao.mostrarDialogoJogadorStackCompra(jogadorDaVez.getNome(), ultimaJogada.getNome(), ultimaJogada.getCor(), indiceCarta != -1, numCartasAComprar);

                if (indiceCarta != -1) { //se o jogador tiver outro +2 ou outro +4 para o stack de compra, ele a joga
                    return jogadorDaVez.jogar(indiceCarta);
                } else { //caso contrário compra a quantidade de cartas acumulada no stack (dentro do switch abaixo)
                    opcao = "c";
                }
            } else { //se a última jogada não envolveu cartas +2 ou +4
                opcao = visao.mostrarDialogoJogador(ultimaJogada.getNome(), corAtual, jogadorDaVez.getNome(), maoJogadorDaVez,
                        temCartaValida, opcaoValida, ultimaJogadaValida);
            }

            //resetando as flags para evitar mensagens incoerentes na próxima iteração
            opcaoValida = true;
            ultimaJogadaValida = true;

            switch (opcao) {
                case "c":
                    if (numCartasAComprar == 0) { //se a compra NÃO é por um stack de cartas +2 ou +4
                        Carta compra = Baralho.getInstance().comprar();
                        jogadorDaVez.comprar(compra);

                        if (validarJogada(compra) && !temCartaValida) { //se o jogador comprou porque não tinha cartas válidas
                            if (jogadorDaVez.temCartaCompativel(ultimaJogada)) { //se a carta comprada for válida para a jogada
                                visao.mostrarDialogoCompraCarta(compra.toString()); 

                                return jogadorDaVez.jogar(jogadorDaVez.getNumCartasNaMao() - 1); //jogue a carta
                            }
                        }
                    } else { //se a compra É por um stack de cartas +2 ou +4
                        for (int j = 0; j < numCartasAComprar; j++) { //compre a quantidade de cartas acumuladas no stack
                            jogadorDaVez.comprar(Baralho.getInstance().comprar());
                        }
                    }
                    numCartasAComprar = 0;
                    opcaoValida = true;
                    break;

                default: //se foi digitado um número ou algo diferente de 'c'
                    try {
                        indiceCarta = Integer.valueOf(opcao) - 1; //retira um para encaixar aos índices do ArrayList (0 a arrayList.size()-1)

                        if (indiceCarta < 0 || indiceCarta >= jogadorDaVez.getNumCartasNaMao()) {
                            opcaoValida = false;
                        } else {
                            if (validarJogada(jogadorDaVez.getMao().get(indiceCarta))) {
                                return jogadorDaVez.jogar(indiceCarta);
                            }

                            ultimaJogadaValida = false;
                        }

                    } catch (NumberFormatException nfe) {//foi digitado algo diferente de um número ou 'c'
                        opcaoValida = false; //é uma opção inválida
                    }
            }

            maoJogadorDaVez.clear();

        } while (!opcaoValida || !ultimaJogadaValida);

        return null;
    }

    private boolean validarJogada(Carta carta) {
        if (carta.isCartaDeAcao() && (carta.getCor().equals(corAtual) || carta.getCor().equals(""))) {//se é uma carta de ação e tem a mesma cor ou é um coringa
            return true;
        } else if (carta.getCor().equals(corAtual) || carta.getNome().equals(ultimaJogada.getNome())) { //se a carta tem mesma cor ou nome da útima carta jogada
            return true;
        }

        return false;
    }

    private void interpretarJogada(Carta carta) { //executa as ações das cartas de ação do jogo
        ultimaJogada = carta;
        corAtual = carta.getCor();

        if (carta.isCartaDeAcao()) {
            switch (carta.getNome()) {
                case "Coringa": //o jogador escolhe qualquer cor do jogo
                    corAtual = visao.mostrarDialogoEscolhaCor(Baralho.getInstance().getCores());
                    break;

                case "+4":
                    numCartasAComprar += 4; //adiciona mais quatro cartas para o proximo jogador comprar
                    corAtual = visao.mostrarDialogoEscolhaCor(Baralho.getInstance().getCores()); //o jogador escolhe qualquer cor do jogo
                    break;

                case "+2":
                    numCartasAComprar += 2; //adiciona mais duas cartas para o proximo jogador comprar
                    break;

                case "Reverter":
                    sentidoHorario = !sentidoHorario; //inverte o sentido do jogo
                    break;

                case "Pular":
                    if (jogadorDaVez.getNumCartasNaMao() == 1) {
                        visao.gritarUNO(jogadorDaVez.getNome());
                    }

                    if (jogadorDaVez.getNumCartasNaMao() != 0) {
                        proximoJogador(); //passa a vez ao proximo jogador, que vai ser pulado no metodo jogo
                    }
                    break;
            }
        }
    }
}
