package Controle;

import Visao.Visao;
import Visao.VisaoTerminal;

public class Main {

    public static void main(String[] args) {
        Visao visao = new VisaoTerminal();
        boolean jogoRapido;

        visao.mostrarMsgBoasVindas();

        jogoRapido = visao.selecionarTipoDeJogo();
        Jogo jogo = new Jogo(jogoRapido, visao);

        jogo.partida();
    }
}
